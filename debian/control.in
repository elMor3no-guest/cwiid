Source: cwiid
Section: utils
Priority: optional
Maintainer: Romain Beauxis <toots@rastageeks.org>
Build-Depends: bison,
               dh-support,
               flex,
               libbluetooth-dev,
               libgtk2.0-dev,
               python3:any |
               python3-all:any |
               python3-dev:any |
               python3-all-dev:any |
               dh-sequence-python3,
               python-all-dev,
               @cdbs@
Standards-Version: 4.5.0
Homepage: http://abstrakraft.org/cwiid/
Rules-Requires-Root: no

Package: python3-cwiid
Architecture: any
Depends: libcwiid1 (>= ${binary:Version}),
         ${misc:Depends},
         ${python:Depends},
         ${shlibs:Depends}
Section: python
Description: library to interface with the wiimote
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides the Python cwiid module.

Package: libcwiid1
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Section: libs
Description: library to interface with the wiimote -- runtime files
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides the wiimote library that abstracts the interface
 to the wiimote by hiding the details of the underlying Bluetooth
 connection.

Package: libcwiid-dev
Architecture: any
Depends: libbluetooth-dev,
         libcwiid1 (= ${binary:Version}),
         pkg-config,
         ${misc:Depends}
Conflicts: libcwiid0-dev
Replaces: libcwiid0-dev,
          libcwiid1-dev
Section: libdevel
Description: library to interface with the wiimote -- developpement
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides the developpement files needed for building against
 Cwiid.

Package: lswm
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: wiimote discover utility
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides an utility to discover new wiimotes.

Package: wmgui
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: GUI interface to the wiimote
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides the GUI to test and display the wiimote data.

Package: wminput
Architecture: any
Depends: python3-cwiid,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Userspace driver for the wiimote
 CWiid is a working userspace driver along with various
 applications implementing event drivers, multiple wiimote
 connectivity, gesture recognition, and other Wiimote-based
 functionality.
 .
 This package provides an event driver for the wiimote, supporting all
 buttons (except Power) and pointer tracking, and featuring a tracking
 algorithm plugin architecture.
